declare module 'response-models' {
	import { AxiosResponse } from 'axios';

	type User = {
		address: string;
		address_2: string;
		email: string;
		phone: string;
		thumbnail: {
			name_image: string;
			url_image: string;
		};
		username: string;
		uuid: string;
	};
	export type UserAccountResponse = MessageResponse & {
		user: User | null;
	};

	export type UserDetailsResponse = MessageResponse & {
		user: {
			email: string;
			thumbnail: {
				name_image: string;
				url_image: string;
			};
			username: string;
			relations: {
				posts: Post[];
			};
		};
	};

	export type CommentResponse = {
		content: string;
		created_at: string;
		published: string;
		relations: {
			post: {
				id: number;
				title: string;
			};
			user: {
				email: string;
				id: number;
				thumbnail: string;
				username: string;
			};
		};
	};

	export type Post = {
		id: string;
		content: string;
		created_at: string;
		title: string;
		image: {
			name_image: string;
			url_image: string;
		};
		post_uuid: string;
		relations: {
			user: {
				email: string;
				id: number;
				thumbnail: string;
				username: string;
			};
			comments: CommentResponse[];
		};
	};
	export type PostListResponse = {
		posts: Post[];
		total_posts: number;
	};

	export type PostResponse = MessageResponse & { post: Post };
	export type PostDeleteResponse = MessageResponse & {
		post_title: string;
		post_id: string;
	};

	export type MessageResponse = {
		message: object | string | string[] | object[];
	};

	export type ResponseApi<T = MessageResponse> = Promise<AxiosResponse<T>>;
}
