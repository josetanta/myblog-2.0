declare module 'required-params' {
	export type LoginData = { email: string; password: string };
}

declare module 'store-app' {
	import { UserAccountResponse } from 'response-models';
	import { LoginData } from 'required-params';
	type UserRegister = {
		username: string;
		email: string;
		password: string;
		repeatPassword: string;
	};

	export type AuthStore = {
		user: UserAccountResponse['user'] | null;
		isAuthenticated: boolean;
		isLoading?: boolean;
		isError?: boolean;
		message?: object | string | string[] | object[];
		isReady?: boolean;
		authName?: string;
		isUpdated?: boolean;

		setIsReady(value: boolean, authName?: string): void | Promise<void>;
		setLoginAuth(data: LoginData): Promise<void>;
		setLogoutAuth(): Promise<void>;
		setUpdateAccount(value: any | object): Promise<void>;
		setRegisterUser(data: UserRegister): Promise<void>;
		setReset(): void;
		setResetResponse(): void;
		setLogin0Auth(): Promise<void>;
		getUserAccount(): Promise<void>;
	};
}
