module.exports = {
	extension: ['d.ts', 'ts', 'tsx'],
	recursive: true,
	slow: 75,
	timeout: 5000,
	spec: ['src/__tests__/*.spec.tsx', 'src/__tests__/*.test.tsx'],
	require: ['ts-node/register', 'webpack.dev.js', 'src/setupTests.ts'],
};
// env TS_NODE_COMPILER_OPTIONS='{\"module\": \"commonjs\" }'
