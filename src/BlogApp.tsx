import GeneralProvider from 'providers/GeneralProvider';
import CheckConnection from 'components/CheckConnection';
import AppRouter from 'router';
import withStrictMode from 'wrappers/withStrictMode';
import Root from 'components/Root';

function BlogApp() {
	return (
		<GeneralProvider>
			<CheckConnection>
				<Root>
					<AppRouter />
				</Root>
			</CheckConnection>
		</GeneralProvider>
	);
}

export default withStrictMode(BlogApp);
