import { LoginData } from 'required-params';
import { MessageResponse, ResponseApi, UserAccountResponse } from 'response-models';

import { clientHttp } from './config';

export async function authLogin(data: LoginData): ResponseApi {
	try {
		return await clientHttp.post('/login/', data);
	} catch (error: any) {
		return error.response;
	}
}

export async function authLogout(authName?: string): ResponseApi {
	try {
		return await clientHttp.post<MessageResponse>(
			authName ? `/auth/logout/${authName}` : '/logout/'
		);
	} catch (error: any) {
		return error.response;
	}
}

export async function authGetUserAccount(): ResponseApi<UserAccountResponse> {
	try {
		return await clientHttp.get<UserAccountResponse>('/users/account/');
	} catch (error: any) {
		return error.response;
	}
}

export async function authRegister(data: any): ResponseApi {
	try {
		return await clientHttp.post<MessageResponse>('/register/', data);
	} catch (error: any) {
		return error.response;
	}
}

export async function getTokenAuth() {
	return await clientHttp.get(`/auth/token`);
}
