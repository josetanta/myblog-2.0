import {
	Post,
	PostDeleteResponse,
	PostListResponse,
	PostResponse,
	ResponseApi,
} from 'response-models';
import { clientHttp } from './config';

export async function postGetList(): Promise<PostListResponse> {
	const res = await clientHttp.get<PostListResponse>('/posts/');
	return res.data;
}

export async function postGetDetail(postId: string): Promise<PostResponse> {
	const res = await clientHttp.get<PostResponse>(`/posts/${postId}`);
	return res.data;
}

export async function createPost(data: object | FormData | Post): ResponseApi<PostResponse> {
	try {
		return await clientHttp.post<PostResponse>('/posts/', data);
	} catch (error: any) {
		return error.response;
	}
}

export async function removePost(postId: string): ResponseApi<PostDeleteResponse> {
	return await clientHttp.delete(`/posts/deleted/?post_id=${postId}&deleted=1`);
}
