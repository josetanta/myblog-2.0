import axios from 'axios';

const API_URL = process.env.REACT_APP_API_URL || '';

// Config Default
axios.defaults.withCredentials = true;
axios.defaults.baseURL = `${API_URL}/api/v1`;
axios.defaults.headers.post['Content-Type'] = 'application/json;';
axios.defaults.headers.patch['Content-Type'] = 'application/json;';
axios.defaults.headers.post['Accept'] = 'application/vnd.heroku+json; version=3';
axios.defaults.headers.put['Accept'] = 'application/vnd.heroku+json; version=3';
axios.defaults.headers.patch['Accept'] = 'application/vnd.heroku+json; version=3';
axios.defaults.xsrfCookieName = 'jwt';

export const clientHttp = axios.create();
