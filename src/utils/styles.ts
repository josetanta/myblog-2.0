import { alpha, Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/styles';

import * as colors from '@material-ui/core/colors';
type StylesProps = { imgWidth?: string; imgHeight?: string };
const useStylesApp = makeStyles<Theme, StylesProps>((theme) =>
	createStyles({
		grow: {
			flexGrow: 1,
		},
		mLeft: {
			marginLeft: 20,
		},

		linkDrawer: {
			display: 'flex',
			justifyContent: 'space-between',
			alignItems: 'center',
			textDecoration: 'none',
			color: theme.palette?.primary?.main,
		},

		imgAccount: {
			borderRadius: '5px',
			width: 'calc(100% - 10rem)',
			alignSelf: 'center',
			margin: '2rem auto',
			backgroundSize: 'cover',
			backgroundRepeat: 'no-repeat',
			backgroundAttachment: 'fixed',
		},

		metaInfoAccount: {
			padding: '8px 18px',
		},

		postsScroll: {
			overflowY: 'scroll',
			height: '100vh',
			borderTop: `1px solid #000`,
		},

		activeItem: {
			backgroundColor: 'red',
		},

		formFileLabel: {
			cursor: 'pointer',
			backgroundColor: '#fff',
			color: '#000',
			textAlign: 'center',
		},
		formFile: {
			opacity: 0,
			position: 'absolute',
			zIndex: -1,
		},

		imgDrag: {
			alignSelf: 'center',
			width: (props) => props.imgWidth || '95%',
			height: (props) => props.imgHeight || '95%',
		},

		iconCloseFile: {
			position: 'absolute',
			top: 10,
			right: 1,
		},

		editorRich: {
			display: 'flex',
			flexGrow: 1,
			justifyContent: 'space-between',
			backgroundColor: alpha(colors.green['100'], 0.3),
			padding: 5,
			mb: 2,
		},
	})
);

export default useStylesApp;
