import { createTheme, responsiveFontSizes } from '@material-ui/core/styles';

let appTheme = createTheme({
	palette: {
		primary: {
			main: '#808080',
			contrastText: '#FFFFFF',
		},
	},
	typography: {
		fontFamily: ['Poppins'].join(','),
	},
});

appTheme = responsiveFontSizes(appTheme);

export default appTheme;
