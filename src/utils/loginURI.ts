const LOGIN_0AUTH = `${process.env.REACT_APP_API_URL}/api/v1/login/auth/`;

export function loginAuthURI(nameProvider: string): void {
	window.location.replace(LOGIN_0AUTH + nameProvider);
}
