import { Box, Typography } from '@material-ui/core';
import Layout from 'components/Layout';
import AccountForm from 'components/ui/AccountForm';
import AlertMessage from 'components/ui/AlertMessage';
import UserToolbarLayout from 'components/user/UserToolbarLayout';
import { useAuth } from 'hooks/useStores';

function AccountConfigPage() {
	const [auth, getState] = useAuth();
	return (
		<Layout title='Configurar mi cuenta'>
			<UserToolbarLayout>
				<Box
					sx={{
						height: '100%',
					}}
					display='flex'
					flexDirection='column'
					justifyContent='space-evenly'
				>
					<Typography
						component='h3'
						variant='h3'
						sx={{
							px: 3,
						}}
					>
						Actualizar mi cuenta de usuario
					</Typography>
					{getState().isError && (
						<AlertMessage
							message={getState()?.message}
							show={getState()?.isError}
							onClose={auth.setResetResponse}
							severity='error'
						/>
					)}
					{getState().isUpdated && (
						<AlertMessage
							message={getState()?.message}
							show={getState()?.isUpdated}
							onClose={auth.setResetResponse}
							severity='success'
						/>
					)}
					<AccountForm />
				</Box>
			</UserToolbarLayout>
		</Layout>
	);
}

export default AccountConfigPage;
