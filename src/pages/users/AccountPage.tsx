import { RouteComponentProps } from 'react-router-dom';

import { Box, Button, Table, TableBody, TableCell, TableRow, Typography } from '@material-ui/core';
import Layout from 'components/Layout';
import { useAuth } from 'hooks/useStores';

import * as paths from 'router/paths-routes';
import UserToolbarLayout from 'components/user/UserToolbarLayout';

function AccountPage(props: RouteComponentProps) {
	const history = props.history;
	const [auth] = useAuth();
	const handlePushRouteClick = () => {
		history.push(`${paths.USER_PRINCIPAL}${paths.USER_SETTINGS}`);
	};

	return (
		<Layout title='Perfil'>
			<UserToolbarLayout>
				<Box
					sx={{
						height: '100vh',
					}}
					display='flex'
					flexDirection='column'
					justifyContent='space-evenly'
				>
					<Typography
						component='h3'
						variant='h3'
						sx={{
							px: 3,
						}}
					>
						Información de mi cuenta
					</Typography>
					<Table>
						<TableBody>
							<TableRow>
								<TableCell
									sx={{
										fontWeight: 'bolder',
										paddingTop: 5,
									}}
									align='left'
								>
									Nombre completo
								</TableCell>
								<TableCell
									sx={{
										paddingTop: 5,
									}}
									align='left'
								>
									{auth.user?.username}
								</TableCell>
							</TableRow>
							<TableRow>
								<TableCell
									sx={{
										fontWeight: 'bolder',
										paddingTop: 5,
									}}
									align='left'
								>
									Usuario
								</TableCell>
								<TableCell
									sx={{
										paddingTop: 5,
									}}
									align='left'
								>
									{auth.user?.username}
								</TableCell>
							</TableRow>
							<TableRow>
								<TableCell
									sx={{
										fontWeight: 'bolder',
										paddingTop: 5,
									}}
									align='left'
								>
									Correo
								</TableCell>
								<TableCell
									sx={{
										paddingTop: 5,
									}}
									align='left'
								>
									{auth.user?.email}
								</TableCell>
							</TableRow>
							<TableRow>
								<TableCell
									sx={{
										fontWeight: 'bolder',
										paddingTop: 5,
									}}
									align='left'
								>
									Teléfono
								</TableCell>
								<TableCell
									sx={{
										paddingTop: 5,
									}}
									align='left'
								>
									{auth.user?.phone}
								</TableCell>
							</TableRow>
						</TableBody>
					</Table>
					<Button color='inherit' onClick={handlePushRouteClick}>
						Editar mi cuenta
					</Button>
				</Box>
			</UserToolbarLayout>
		</Layout>
	);
}

export default AccountPage;
