import { RouteComponentProps } from 'react-router-dom';
import { useUserSettings } from 'context/SettingsContext';
import { usePostRemove, useUserDetails } from 'hooks/usePostQuery';
import { useAuth } from 'hooks/useStores';
import {
	alpha,
	Box,
	Button,
	Card,
	CardActionArea,
	CardActions,
	CardContent,
	CardHeader,
	CardMedia,
	IconButton,
	Table,
	TableBody,
	TableCell,
	TableHead,
	TableRow,
	Typography,
	useMediaQuery,
} from '@material-ui/core';
import Layout from 'components/Layout';
import AlertMessage from 'components/ui/AlertMessage';
import UserToolbarLayout from 'components/user/UserToolbarLayout';
import * as colors from '@material-ui/core/colors';
import * as paths from 'router/paths-routes';

// Icons
import GridViewIcon from '@material-ui/icons/GridView';
import ViewHeadlineIcon from '@material-ui/icons/ViewHeadline';
import PostAddIcon from '@material-ui/icons/PostAdd';
import DeleteIcon from '@material-ui/icons/Delete';

import useStylesApp from 'utils/styles';

function MyPostsPage(props: RouteComponentProps) {
	const classes = useStylesApp({});
	const getState = useAuth()[1];
	const userSettings = useUserSettings();
	const postRemove = usePostRemove({ changeStatus: true, userId: getState().user?.uuid });
	const userDetails = useUserDetails(getState().user?.uuid || '');
	const mobileSize = useMediaQuery('(max-width:900px)');

	const posts = userDetails.data?.user.relations?.posts;

	const handleCreatePostRedirectClick = () => {
		props.history.push(`${paths.POST_PRINCIPAL}${paths.POST_CREATE}`);
	};

	const handlePostDetailClick = (postId: string) => {
		props.history.push(`${paths.POST_PRINCIPAL}/detail/${postId}`);
	};

	return (
		<Layout title='Mis Publicaciones'>
			<UserToolbarLayout>
				<Box
					sx={{
						height: '100%',
					}}
				>
					<Box
						sx={{
							mb: 3,
						}}
						display='flex'
						justifyContent='space-around'
						alignItems='center'
					>
						<Typography variant='h5' component='h5'>
							Todas mis publicaciones
						</Typography>
						<div>
							<IconButton
								onClick={handleCreatePostRedirectClick}
								color='success'
								title='Crear una publicación'
							>
								<PostAddIcon />
							</IconButton>
							{(!userSettings.state.isGridView && (
								<IconButton
									sx={{
										backgroundColor: alpha(colors.blueGrey['200'], 0.53),
									}}
									onClick={() => userSettings.dispatch({ type: 'CHANGE_VIEW_POSTS' })}
								>
									<GridViewIcon />
								</IconButton>
							)) || (
								<IconButton
									sx={{
										backgroundColor: alpha(colors.blueGrey['200'], 0.53),
									}}
									onClick={() => userSettings.dispatch({ type: 'CHANGE_VIEW_POSTS' })}
								>
									<ViewHeadlineIcon />
								</IconButton>
							)}
						</div>
					</Box>
					{postRemove.isSuccess && (
						<Box display='flex' justifyContent='center' marginY={'5px'} padding={'5px'}>
							<AlertMessage
								show={postRemove.isSuccess}
								message={postRemove.data?.data.message}
								onClose={postRemove.reset}
								severity='info'
							/>
						</Box>
					)}
					{userSettings.state.isGridView ? (
						<Box display='flex' flexWrap='wrap' className={classes.postsScroll}>
							{posts?.map((post, index) => (
								<Card
									elevation={2}
									key={`${post.title[5].toLowerCase()}-${index}`}
									sx={{
										my: 2,
										mx: 1,
										height: 400,
										width: mobileSize ? '100%' : 280,
										position: 'relative',
									}}
								>
									<CardActionArea onClick={() => handlePostDetailClick(post.id)}>
										<CardHeader title={post.title} subheader={post.created_at} />
										<CardMedia
											image={post.image.url_image}
											title={post.title}
											sx={{
												height: 105,
												width: 250,
											}}
										/>
										<CardContent>
											<Typography
												variant='body2'
												dangerouslySetInnerHTML={{ __html: post.content.slice(0, 50) }}
											/>
										</CardContent>
									</CardActionArea>
									<CardActions
										sx={{
											justifyContent: 'space-between',
											position: 'absolute',
											width: '100%',
											bottom: 0,
											bgcolor: '#f1f1f1',
										}}
									>
										<Button onClick={() => handlePostDetailClick(post.id)}>Ver</Button>
										<IconButton color='error' onClick={() => postRemove.mutate(post.id)}>
											<DeleteIcon />
										</IconButton>
									</CardActions>
								</Card>
							))}
						</Box>
					) : (
						<Table>
							<TableHead
								sx={{
									fontWeight: 'bold',
								}}
							>
								<TableRow>
									<TableCell>Imagen</TableCell>
									<TableCell>Titulo</TableCell>
									<TableCell>Contenido</TableCell>
									<TableCell>Acciones</TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{posts?.map((post, index) => (
									<TableRow key={`${post.title[2].toLowerCase}-${index + 1}`}>
										<TableCell>
											<img src={post.image.url_image} alt={post.title} width={99} />
										</TableCell>
										<TableCell>{post.title}</TableCell>
										<TableCell>
											<div dangerouslySetInnerHTML={{ __html: post.content.slice(0, 50) }} />
										</TableCell>
										<TableCell>
											<Button onClick={() => handlePostDetailClick(post.id)}>Ver</Button>
											<IconButton color='error' onClick={() => postRemove.mutate(post.id)}>
												<DeleteIcon />
											</IconButton>
										</TableCell>
									</TableRow>
								))}
							</TableBody>
						</Table>
					)}
				</Box>
			</UserToolbarLayout>
		</Layout>
	);
}

export default MyPostsPage;
