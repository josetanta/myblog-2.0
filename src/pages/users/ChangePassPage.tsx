import { Box, Typography } from '@material-ui/core';
import Layout from 'components/Layout';
import ChangePasswordForm from 'components/ui/ChangePasswordForm';
import UserToolbarLayout from 'components/user/UserToolbarLayout';

function ChangePassPage() {
	return (
		<Layout>
			<UserToolbarLayout>
				<Box
					sx={{
						height: '100vh',
					}}
					display='flex'
					flexDirection='column'
					justifyContent='space-evenly'
				>
					<Typography
						variant='h3'
						component='h3'
						sx={{
							px: 3,
						}}
					>
						Cambiar mi contraseña
					</Typography>
					<ChangePasswordForm />
				</Box>
			</UserToolbarLayout>
		</Layout>
	);
}

export default ChangePassPage;
