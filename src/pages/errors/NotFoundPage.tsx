import { Box, Button, Paper, Typography } from '@material-ui/core';
import { RouteComponentProps } from 'react-router-dom';

// Icons
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';

function NotFoundPage(props: RouteComponentProps<Record<string, string>>) {
	const rest = props.match.params.rest;

	const handleBackRouteClick = () => {
		props.history.goBack();
	};

	return (
		<Box sx={{ p: 10 }}>
			<Paper sx={{ p: 5, m: '5rem auto', bgcolor: '#f1f1f1' }} elevation={3}>
				<Box display='flex' flexDirection='column' justifyContent='center' alignItems='center'>
					<Typography sx={{ mb: 10 }} component='h1' variant='h1'>
						Esta página no esta disponible o no existe
					</Typography>
					<Typography sx={{ mb: 10 }} component='h3' variant='h3' color='error'>
						{JSON.stringify(rest)}
					</Typography>
					<Button
						startIcon={<ArrowBackIosIcon />}
						onClick={handleBackRouteClick}
						variant='contained'
					>
						Regresar
					</Button>
				</Box>
			</Paper>
		</Box>
	);
}

export default NotFoundPage;
