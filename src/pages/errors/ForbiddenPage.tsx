import { RouteComponentProps } from 'react-router-dom';
import { Box, Button, Paper, Typography } from '@material-ui/core';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';

function ForbiddenPage(props: RouteComponentProps) {
	const handleBackRouteClick = () => {
		props.history.goBack();
	};

	return (
		<Box sx={{ p: 10 }}>
			<Paper sx={{ p: 5, m: '5rem auto' }} elevation={3}>
				<Box display='flex' flexDirection='column' justifyContent='center' alignItems='center'>
					<Typography sx={{ mb: 10 }} component='h1' variant='h1'>
						Usted no tiene permisos para acceder a esta dirección
					</Typography>
					<Button
						startIcon={<ArrowBackIosIcon />}
						onClick={handleBackRouteClick}
						variant='contained'
					>
						Regresar
					</Button>
				</Box>
			</Paper>
		</Box>
	);
}

export default ForbiddenPage;
