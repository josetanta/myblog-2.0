// Components
import Layout from 'components/Layout';
import { Typography } from '@material-ui/core';
import ListPost from 'components/ListPost';

function PostPrincipal() {
	return (
		<Layout title='Publicaciones'>
			<Typography variant='h1' component='h1' align='center'>
				Publicaciones
			</Typography>
			<ListPost />
		</Layout>
	);
}

export default PostPrincipal;
