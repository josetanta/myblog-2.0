import { Fragment } from 'react';
import { usePostDetail } from 'hooks/usePostQuery';
import { RouteComponentProps } from 'react-router-dom';

import Layout from 'components/Layout';
import { Box, Container, Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { motion } from 'framer-motion';
import CommentList from 'components/CommentList';

const useStyles = makeStyles({
	imgPost: {
		'& > img': {
			maxWidth: 700,
			minWidth: 300,
			width: '100%',
			backgroundSize: 'cover',
			backgroundRepeat: 'no-repeat',
		},
	},
});

function PostDetailPage(props: RouteComponentProps<Record<string, string>>) {
	const postId = props.match.params.postId;
	const post = usePostDetail(postId);
	const classes = useStyles();

	return (
		<Layout title='Publicación Detalle'>
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1 }}
				transition={{
					delay: 1,
					x: {
						type: 'spring',
						stiffness: 100,
					},
					default: { duration: 1 },
				}}
			>
				<Container maxWidth='xl'>
					<Typography
						variant='h1'
						component='h1'
						align='center'
						color='primary'
						sx={{
							fontWeight: 'bold',
						}}
					>
						{post.data?.post.title}
					</Typography>
					<Box display='flex' justifyContent='center' className={classes.imgPost}>
						<img alt={post.data?.post.title} src={post.data?.post.image.url_image} />
					</Box>

					<Paper
						elevation={3}
						sx={{
							p: 5,
							letterSpacing: 0.6,
							lineHeight: 2,
						}}
					>
						<Typography
							component='div'
							variant='body1'
							textAlign='justify'
							dangerouslySetInnerHTML={{ __html: post.data?.post.content || '' }}
						/>
					</Paper>
				</Container>
				{post.data?.post.relations?.comments && (
					<Fragment>
						<Typography sx={{ mt: 5 }} variant='h6' align='center'>
							Comentarios
						</Typography>
						<CommentList comments={post.data?.post.relations?.comments} />
					</Fragment>
				)}
			</motion.div>
		</Layout>
	);
}

export default PostDetailPage;
