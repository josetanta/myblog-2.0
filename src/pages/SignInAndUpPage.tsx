import { ReactNode, SyntheticEvent, useState } from 'react';
import { Redirect, RouteComponentProps } from 'react-router-dom';
import { Box, Button, Tab, Tabs, Typography } from '@material-ui/core';
import Layout from 'components/Layout';

import LoginForm from 'components/ui/LoginForm';
import RegisterForm from 'components/ui/RegisterForm';
import * as paths from 'router/paths-routes';

import * as colors from '@material-ui/core/colors';

// Icons
import GithubIcon from '@material-ui/icons/GitHub';
import GoogleIcon from '@material-ui/icons/Google';
// Function
import { loginAuthURI } from 'utils/loginURI';
import { useAuth } from 'hooks/useStores';

type TabPanelProps = {
	children?: ReactNode;
	index: number;
	value: number;
	href?: string;
};

function TabPanel(props: TabPanelProps) {
	const { index, value, children, href, ...other } = props;

	return (
		<div
			role='tabpanel'
			hidden={value !== index}
			id={`tabpanel-${index}`}
			aria-labelledby={`tabpanel-${index}`}
			{...other}
		>
			{value === index && <Box sx={{ p: 3 }}>{children}</Box>}
		</div>
	);
}

const a11yProps = (index: number) => ({
	id: `tab-panel-${index}`,
	'aria-controls': `tabpanel-${index}`,
});

function SignInAndUpPage(props: RouteComponentProps) {
	const [auth] = useAuth();
	const location = props.location;
	const params = props.match.params as Record<string, string>;

	const [value, setValue] = useState(() => {
		if (params.type === 'in') {
			return 0;
		} else if (params.type === 'up') {
			return 1;
		}
		return 0;
	});

	const handleTabChange = (event: SyntheticEvent, newValue: number) => {
		event.preventDefault();
		setValue(newValue);
	};

	const handleLogin0AuthClick = (s: string) => {
		auth.setIsReady(true, s);
		loginAuthURI(s);
	};

	if (auth.isAuthenticated) {
		return (
			<Redirect
				exact
				from={paths.SIGN_IN}
				to={{ pathname: paths.HOME, state: { from: location } }}
			/>
		);
	}

	return (
		<Layout title=''>
			<Box display='flex' sx={{ width: '100%' }} justifyContent='center'>
				<Tabs value={value} onChange={handleTabChange} aria-label='tabs-register-and-login'>
					<Tab label='Iniciar Sesión' {...a11yProps(0)} />
					<Tab label='Registrarse' {...a11yProps(1)} />
				</Tabs>
			</Box>
			<TabPanel value={value} index={0}>
				<Typography variant='h4' component='h4' align='center'>
					Iniciar Sesión
				</Typography>
				<LoginForm />

				<Box display='flex' flexDirection='column' justifyContent='space-between' marginY='2rem'>
					<Typography variant='h5' component='h5' align='center'>
						O
					</Typography>
					<Button
						onClick={() => handleLogin0AuthClick('google')}
						startIcon={<GoogleIcon />}
						sx={{
							my: 2,
							backgroundColor: '#4285F4',
							color: '#fff',
							':hover': {
								backgroundColor: '#C0D6FB',
								color: '#6F7277',
							},
						}}
					>
						Google
					</Button>
					<Button
						onClick={() => handleLogin0AuthClick('github')}
						startIcon={<GithubIcon />}
						sx={{
							my: 2,
							backgroundColor: colors.grey['900'],
							color: '#fff',
							transition: '0.3s all',
							':hover': {
								color: colors.grey['900'],
								backgroundColor: colors.grey['500'],
							},
						}}
					>
						Github
					</Button>
					<Button
						onClick={() => handleLogin0AuthClick('spotify')}
						startIcon={<i className='bx bxl-spotify' />}
						sx={{
							my: 2,
							backgroundColor: '#2BDE6A',
							color: '#000000',
							transition: '0.3s all',
							':hover': {
								backgroundColor: '#71E99B',
								color: '#000000',
							},
						}}
					>
						Spotify
					</Button>
				</Box>
			</TabPanel>
			<TabPanel value={value} index={1}>
				<Typography variant='h4' component='h4' align='center'>
					Registrarse
				</Typography>
				<RegisterForm />
			</TabPanel>
		</Layout>
	);
}

export default SignInAndUpPage;
