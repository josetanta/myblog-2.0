import { createContext, ReactNode, useContext, useReducer } from 'react';
import { SettingsActions, settingsReducer, SettingsState } from './reducers';

type SettingsContextType = {
	state: SettingsState;
	dispatch(value: SettingsActions): void;
};
const settingsContext = createContext<SettingsContextType>({} as SettingsContextType);

const initState: SettingsState = {
	file: undefined,
	isGridView: true,
};

export function SettingsProvider(props: { children: ReactNode }) {
	const [state, dispatch] = useReducer(settingsReducer, initState);

	return (
		<settingsContext.Provider
			value={{
				state,
				dispatch,
			}}
		>
			{props.children}
		</settingsContext.Provider>
	);
}

export const SettingsConsumer = settingsContext.Consumer;

export function useUserSettings() {
	return useContext(settingsContext);
}
