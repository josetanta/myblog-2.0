export type SettingsState = {
	file?: File;
	isGridView: boolean;
};

export type SettingsActions =
	| {
			type: 'SUBMIT_FILE';
			payload: File | undefined;
	  }
	| {
			type: 'REMOVE_FILE';
	  }
	| {
			type: 'CHANGE_VIEW_POSTS';
	  };


export function settingsReducer(state: SettingsState, action: SettingsActions) {
	switch (action.type) {
		case 'SUBMIT_FILE':
			return { ...state, file: action?.payload };

		case 'REMOVE_FILE':
			return { ...state, file: undefined };

		case 'CHANGE_VIEW_POSTS':
			return { ...state, isGridView: !state.isGridView };

		default:
			return state;
	}
}
