import { shallow, ShallowWrapper } from 'enzyme';
import { expect } from 'chai';

// Component
import Comment from '../components/Comment';
import { CardHeader, Typography } from '@material-ui/core';

const data = {
	content: 'Contenido del comentario',
	created_at: '',
	published: '',
	relations: {
		post: { id: 0, title: '' },
		user: {
			email: '',
			id: 0,
			thumbnail: '',
			username: 'nombre de usuario',
		},
	},
};

describe('Render component components/Comment.tsx', () => {
	let wrapper: ShallowWrapper;
	beforeEach(() => {
		wrapper = shallow(<Comment data={data} />);
	});

	afterEach(() => {
		wrapper.unmount();
	});

	it('Should mounted component and view content itself', () => {
		expect(wrapper.contains(data.content)).to.eq(true);
	});

	it('Should view elements of Material-ui', () => {
		expect(wrapper.find(Typography)).to.be.present();
		expect(wrapper.find(CardHeader)).to.be.present();
	});
});
