import 'react/jsx-runtime';

import { shallow, ShallowWrapper } from 'enzyme';
import { expect } from 'chai';
import Post from 'components/Post';
import sinon, { SinonSpy } from 'sinon';
import { Card, CardActionArea, CardActions, CardContent, CardMedia } from '@material-ui/core';

const data = {
	content: 'Content del post',
	created_at: '',
	id: '1',
	image: {
		name_image: 'wqewqeqw',
		url_image:
			'https://www.bing.com/images/search?view=detailV2&ccid=o%2f%2bISy7t&id=5356FA6C6A83594E7109A4FF00C29CD2B9DE59A1&thid=OIP.o_-ISy7twdwOsXv7ju28TQHaQd&mediaurl=https%3a%2f%2fwww.fonewalls.com%2fwp-content%2fuploads%2f2019%2f09%2fRiver-between-hills-Wallpaper.jpg&cdnurl=https%3a%2f%2fth.bing.com%2fth%2fid%2fR.a3ff884b2eedc1dc0eb17bfb8eedbc4d%3frik%3doVneudKcwgD%252fpA%26pid%3dImgRaw%26r%3d0&exph=2400&expw=1080&q=wallpaper&simid=608027984665602435&FORM=IRPRST&ck=B163D6625393021703D568C3F011DD34&selectedIndex=1',
	},
	post_uuid: '',
	title: '',
	relations: {
		comments: [],
		user: {
			email: '',
			id: 0,
			thumbnail: '',
			username: '',
		},
	},
};

describe('Component src/components/Post.tsx', () => {
	let wrapper: ShallowWrapper;
	let spy: SinonSpy;

	beforeEach(() => {
		spy = sinon.spy();
		wrapper = shallow(<Post data={data} onClick={spy} />);
	});
	afterEach(() => {
		wrapper.unmount();
	});

	it('Mounted component', () => {
		expect(wrapper);
	});

	it('Should render elements', () => {
		expect(wrapper.find(Card)).to.be.present();
		expect(wrapper.find(CardActions)).to.be.present();
		expect(wrapper.find(CardMedia)).to.be.present();
		expect(wrapper.find(CardContent)).to.be.present();
		expect(wrapper.find(CardActionArea)).to.be.present();
	});

	it('Should show the data on render', () => {
		expect(wrapper.contains(data.content)).to.eqls(true);
	});
});
