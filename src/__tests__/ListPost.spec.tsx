import { expect } from 'chai';
import ListPost from 'components/ListPost';
import { shallow, ShallowWrapper } from 'enzyme';

describe('component src/components/ListPost.tsx', () => {
	let wrapper: ShallowWrapper;
	beforeEach(() => {
		wrapper = shallow(<ListPost />);
	});

	afterEach(() => {
		wrapper.unmount();
	});

	it('Mounted component', () => {
		expect(wrapper).to.be.present();
	});
});
