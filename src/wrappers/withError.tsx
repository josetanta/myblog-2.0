import { Component, FC } from 'react';

import { Box, Typography } from '@material-ui/core';

import * as colors from '@material-ui/core/colors';

type ErrorBoundaryState = {
	hasError: boolean;
	message: string;
};

function withError<P = {}>(WrapperComponent: FC<P>) {
	class ErrorBoundary extends Component<P, ErrorBoundaryState> {
		constructor(props: P) {
			super(props);
			this.state = {
				hasError: false,
				message: '',
			};
		}

		componentDidCatch(error: Error) {
			this.setState({ hasError: true, message: error.message });
		}

		render() {
			if (this.state.hasError) {
				return (
					<Box
						border={`1px solid ${colors.red[200]}`}
						display='flex'
						padding='10px'
						color={colors.red[200]}
						justifyContent='center'
						flexWrap='wrap'
					>
						<Typography variant='body1'>{this.state.message}</Typography>
					</Box>
				);
			}

			return <WrapperComponent {...this.props} />;
		}
	}

	return ErrorBoundary;
}

export default withError;
