import create from 'zustand';
import * as authApi from 'api/authApi';
import * as userApi from 'api/userApi';
import { AuthStore } from 'store-app';

const authStore = create<AuthStore>((set, get) => ({
	isAuthenticated: false,
	isReady: false,
	user: null,

	async setLoginAuth(data) {
		set({ isLoading: true });
		const resLogin = await authApi.authLogin(data);
		if (resLogin.status !== 200) {
			set({
				message: resLogin.data.message,
				isError: true,
				isLoading: false,
			});
		} else {
			this.setReset();
			await this.getUserAccount();
		}
	},
	async getUserAccount() {
		let res = await authApi.authGetUserAccount();
		set((state) => {
			if (res.status === 200)
				return {
					...state,
					user: res.data.user,
					isAuthenticated: Boolean(res.data.user),
				};
			else return { ...state };
		});
	},
	async setLogin0Auth() {
		const res = await authApi.authGetUserAccount();
		if (res.status !== 200) {
			set({
				message: res.data.message,
				isError: true,
				isLoading: false,
			});
		} else {
			this.setReset();
			set({
				user: res.data.user,
				isAuthenticated: Boolean(res.data.user),
			});
		}
	},
	async setRegisterUser(data) {
		const res = await authApi.authRegister(data);
		if (res.status !== 201) {
			set({
				message: res.data.message,
				isError: true,
			});
		} else {
			this.setReset();
			set({
				message: res.data.message,
			});
		}
	},

	async setLogoutAuth() {
		await authApi.authLogout(get().authName);
		this.setReset();
	},

	async setUpdateAccount(values) {
		const res = await userApi.accountUpdate(values);
		set({ isLoading: true });
		if (res.status !== 200) {
			set(() => ({
				message: res.data.message,
				isError: true,
				isLoading: false,
			}));
		} else {
			set(() => ({
				user: res.data.user,
				message: res.data.message,
				isError: false,
				isLoading: false,
				isUpdated: true,
			}));
		}
	},

	setIsReady(value) {
		set({ ...get(), isReady: value });
	},

	setResetResponse() {
		set((state) => ({ ...state, isUpdated: false, isError: false, message: '' }));
	},

	setReset() {
		set(() => ({
			user: null,
			isLoading: false,
			isError: false,
			message: '',
			isAuthenticated: false,
			authName: undefined,
		}));
	},
}));

export default authStore;
