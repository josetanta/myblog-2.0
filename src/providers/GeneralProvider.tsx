import { Fragment, ReactNode, useMemo } from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { BrowserRouter } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/core/styles';
import { ReactQueryDevtools } from 'react-query/devtools';
import { SettingsProvider } from 'context/SettingsContext';

import { CssBaseline } from '@material-ui/core';
import appTheme from 'utils/appTheme';

const queryClient = new QueryClient();

const isEnvDev = process.env.NODE_ENV === 'development';

function GeneralProvider(props: { children: ReactNode }) {
	const themeMemo = useMemo(() => appTheme, []);
	return (
		<Fragment>
			<BrowserRouter basename='/'>
				<QueryClientProvider client={queryClient} key='client-provider'>
					<SettingsProvider>
						<ThemeProvider theme={themeMemo}>
							<CssBaseline />
							{props.children}
						</ThemeProvider>
					</SettingsProvider>

					{isEnvDev && <ReactQueryDevtools position='bottom-right' />}
				</QueryClientProvider>
			</BrowserRouter>
		</Fragment>
	);
}

export default GeneralProvider;
