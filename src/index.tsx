import { hydrate, render } from 'react-dom';
import BlogApp from './BlogApp';
import * as serviceWorkerRegistration from './serviceWorkerRegistration';
import reportWebVitals from './reportWebVitals';

const rootDOM = document.getElementById('blog-app') as HTMLElement;
const App = <BlogApp />;

if (rootDOM?.hasChildNodes()) hydrate(App, rootDOM);
else render(App, rootDOM);

serviceWorkerRegistration.unregister();
reportWebVitals();
