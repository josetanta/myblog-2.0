import { AuthStore } from 'store-app';
import shallow from 'zustand/shallow';
import authStore from 'app/authStore';

export function useAuth(): [AuthStore, typeof authStore.getState] {
	return [authStore((state) => state, shallow), authStore.getState];
}
