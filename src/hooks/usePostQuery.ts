import { useMutation, useQuery, useQueryClient } from 'react-query';
import { PostListResponse } from 'response-models';
import * as postApi from 'api/postApi';
import * as userApi from 'api/userApi';
import { queries } from 'utils/queryConstants';

type Options = {
	changeStatus: true;
	userId?: string;
};

export function usePostList() {
	return useQuery(queries.POST_LIST_QUERY, postApi.postGetList, {
		refetchOnWindowFocus: false,
	});
}

export function usePostDetail(postId: string) {
	return useQuery([queries.POST_DETAIL_QUERY, postId], () => postApi.postGetDetail(postId), {
		refetchOnWindowFocus: false,
	});
}

export function usePostRemove(opts: Options) {
	const queryClient = useQueryClient();

	return useMutation((postId: string) => postApi.removePost(postId), {
		onSuccess: async () => {
			await queryClient.cancelQueries([queries.USER_DETAILS_QUERY, opts?.userId]);
			await queryClient.refetchQueries([queries.USER_DETAILS_QUERY, opts?.userId]);
		},
	});
}

export function usePostCreateMutate() {
	const queryClient = useQueryClient();

	return useMutation('CREATE_POST', postApi.createPost, {
		onMutate: async () => {
			await queryClient.cancelQueries(queries.POST_LIST_QUERY);
			await queryClient.refetchQueries([queries.POST_LIST_QUERY]);
			const previousPosts = queryClient.getQueryData<PostListResponse>(queries.POST_LIST_QUERY);
			queryClient.setQueryData<PostListResponse>(queries.POST_LIST_QUERY, (old) => ({
				posts: [...(previousPosts?.posts || [])],
				total_posts: old?.total_posts || 0,
			}));

			return { previousPosts };
		},
		onError: async (_err, _newData, context: any) => {
			queryClient.setQueryData(queries.POST_LIST_QUERY, context.previousPosts);
		},
		onSettled: async () => {
			await queryClient.invalidateQueries(queries.POST_LIST_QUERY);
		},
	});
}

export function useUserDetails(userId: string) {
	return useQuery([queries.USER_DETAILS_QUERY, userId], () => userApi.userDetails(userId), {
		refetchOnWindowFocus: false,
		select: (s) => s.data,
	});
}
