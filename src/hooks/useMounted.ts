import { useCallback, useEffect, useRef } from 'react';

type UseMountedParam = (isMounted: boolean) => Promise<void> | void;

/**
 * Hook mounted on component
 * @param callback function
 * @returns isMounted
 */
function useMounted(callback?: UseMountedParam) {
	const isMountedRef = useRef(true);
	const call = useCallback(() => {
		callback && callback(isMountedRef.current);
	}, [callback]);

	useEffect(() => {
		call();
		return () => {
			isMountedRef.current = false;
		};
	}, [call]);

	return isMountedRef.current;
}

export default useMounted;
