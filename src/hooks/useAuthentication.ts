import { useMutation, useQuery } from 'react-query';
import { authGetUserAccount, authLogin, authLogout } from 'api/authApi';

import { mutations, queries } from 'utils/queryConstants';

export function useLogin() {
	return useMutation(mutations.USER_LOGIN_MUT, authLogin, {
		onMutate: async () => {},
	});
}
export function useLogout() {
	return useMutation(mutations.USER_LOGOUT_MUT, authLogout, {
		onMutate: async () => {},
	});
}
export function useUser() {
	return useQuery(queries.USER_ACCOUNT_QUERY, authGetUserAccount, {
		refetchOnWindowFocus: false,
	});
}
