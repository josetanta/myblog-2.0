import { Fragment, memo, ReactNode, useRef } from 'react';
import { Box, Grid, List, Paper, Typography } from '@material-ui/core';
import { useUserSettings } from 'context/SettingsContext';
import { useAuth } from 'hooks/useStores';
import useStylesApp from 'utils/styles';
import { AnimatePresence, motion } from 'framer-motion';

import * as paths from 'router/paths-routes';

// Icons
import HomeIcon from '@material-ui/icons/Home';
import PostAddIcon from '@material-ui/icons/PostAdd';
import SettingsIcon from '@material-ui/icons/Settings';
import LockIcon from '@material-ui/icons/Lock';
import ItemListNavLink from 'components/ui/ItemListNavLink';
import ArchiveIcon from '@material-ui/icons/Archive';

type UserToolbarLayoutProps = {
	children: ReactNode;
};

const itemLinks = [
	{
		nameItem: 'Principal',
		href: `${paths.USER_PRINCIPAL}${paths.USER_ACCOUNT}`,
		icon: <HomeIcon />,
	},
	{
		nameItem: 'Mis Publicaciones',
		href: `${paths.USER_PRINCIPAL}${paths.USER_MY_POSTS}`,
		icon: <PostAddIcon />,
	},
	{
		nameItem: 'Configuración',
		href: `${paths.USER_PRINCIPAL}${paths.USER_SETTINGS}`,
		icon: <SettingsIcon />,
	},
	{
		nameItem: 'Cambiar mi contraseña',
		href: `${paths.USER_PRINCIPAL}${paths.USER_CHANGE_PASS}`,
		icon: <LockIcon />,
	},
	{
		nameItem: 'Archivados y reciclaje',
		href: `${paths.USER_PRINCIPAL}${paths.USER_ARCHIVE_POSTS}`,
		icon: <ArchiveIcon />,
	},
];

function UserToolbarLayout(props: UserToolbarLayoutProps) {
	const classes = useStylesApp({});
	const getState = useAuth()[1];
	const userSettings = useUserSettings();
	const fileRef = useRef<File>();

	fileRef.current = userSettings.state?.file;

	return (
		<Fragment>
			<Grid container xl={12} sx={{ height: '150vh' }}>
				<Grid item xs={5}>
					<Paper
						elevation={4}
						square
						sx={{
							backgroundColor: '#F6F6F6',
						}}
					>
						<Box
							sx={{
								height: '100vh',
							}}
							display='flex'
							flexDirection='column'
						>
							<Box display='flex'>
								<img
									className={classes.imgAccount}
									src={
										(fileRef.current && URL.createObjectURL(fileRef.current)) ||
										getState().user?.thumbnail.url_image
									}
									alt='My profile'
								/>
							</Box>
							<div className={classes.metaInfoAccount}>
								<Typography
									sx={{
										fontSize: 20,
										fontWeight: 'bold',
									}}
								>
									Nombre del usuario
								</Typography>
								<span>Full name</span>
							</div>
							<List>
								{itemLinks.map((item, index) => (
									<ItemListNavLink
										key={`${item.nameItem.toLowerCase()}-${index + 1}`}
										nameItem={item.nameItem}
										href={item.href}
										icon={item.icon}
									/>
								))}
							</List>
						</Box>
					</Paper>
				</Grid>
				<Grid item xs={7}>
					<Paper elevation={1} square>
						<AnimatePresence>
							<motion.div
								initial={{ opacity: 0 }}
								animate={{ opacity: 1 }}
								transition={{
									delay: 0.5,
									x: {
										type: 'spring',
										stiffness: 50,
									},
									default: { duration: 1 },
								}}
							>
								{props.children}
							</motion.div>
						</AnimatePresence>
					</Paper>
				</Grid>
			</Grid>
		</Fragment>
	);
}

export default memo(UserToolbarLayout);
