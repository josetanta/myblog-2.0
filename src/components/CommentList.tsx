import { Fragment, memo } from 'react';
import { Box } from '@material-ui/core';
import Comment from './Comment';
import { CommentResponse } from 'response-models';

type CommentListProps = {
	comments: CommentResponse[];
};

function CommentList(props: CommentListProps) {
	const comments = props.comments;
	return (
		<Box display='flex' flexDirection='column' justifyContent='center' alignItems='center'>
			{comments.length ? (
				<Fragment>
					{comments.map((comment, index) => (
						<Comment key={`comment-of-${comment.relations.user.id}-${index}`} data={comment} />
					))}
				</Fragment>
			) : null}
		</Box>
	);
}

export default memo(CommentList);
