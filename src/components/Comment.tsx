import { Fragment } from 'react';
import { CommentResponse } from 'response-models';
import { Avatar, Card, CardContent, CardHeader, Typography } from '@material-ui/core';

type CommentProps = {
	data: CommentResponse;
};

function Comment(props: CommentProps) {
	const comment = props.data;
	const user = props.data.relations.user;
	return (
		<Card
			elevation={1}
			sx={{
				my: 2,
				width: 465,
			}}
		>
			<CardHeader
				avatar={
					<Fragment>
						{user.thumbnail !== 'thumbnail.png' && (
							<Avatar alt={user.username} src={user.thumbnail} />
						)}
						{user.thumbnail === 'thumbnail.png' && (
							<Avatar>{user.username[0].toLocaleUpperCase()}</Avatar>
						)}
					</Fragment>
				}
				title={
					<Fragment>
						<Typography
							sx={{
								fontSize: 16,
								fontWeight: 400,
							}}
						>
							{user.username}
						</Typography>
						<Typography variant='body2'>{comment.created_at}</Typography>
					</Fragment>
				}
			/>
			<CardContent>
				<Typography variant='body2' color='text.secondary'>
					{comment.content}
				</Typography>
			</CardContent>
		</Card>
	);
}

export default Comment;
