import { Fragment } from 'react';

import {
	alpha,
	Avatar,
	Button,
	Card,
	CardActionArea,
	CardActions,
	CardContent,
	CardHeader,
	CardMedia,
	IconButton,
	Typography,
} from '@material-ui/core';
import * as colors from '@material-ui/core/colors';

import ShareIcon from '@material-ui/icons/Share';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { PostResponse } from 'response-models';

type PostProps = {
	data: PostResponse['post'];
	onClick(): void;
};

function Post(props: PostProps) {
	const post = props.data;
	const user = post.relations.user;

	return (
		<Card
			sx={{
				maxWidth: 891,
				maxHeight: 450,
				marginY: 2,
				minWidth: 250,
				minHeight: 120,
				position: 'relative',
			}}
			elevation={3}
		>
			<CardActionArea onClick={props.onClick}>
				<CardHeader
					sx={{ bgcolor: colors.grey['200'] }}
					aria-label='publicación'
					avatar={
						<Fragment>
							{user.thumbnail !== 'thumbnail.png' && (
								<Avatar alt={user.username} src={user.thumbnail} />
							)}
							{user.thumbnail === 'thumbnail.png' && (
								<Avatar>{user.username[0].toLocaleUpperCase()}</Avatar>
							)}
						</Fragment>
					}
					title={
						<Typography
							sx={{
								fontSize: 20,
								lineHeight: '24px',
								letterSpacing: 0.15,
								fontWeight: 400,
							}}
						>
							{post.title}
						</Typography>
					}
					subheader={
						<Fragment>
							<Typography
								sx={{
									fontWeight: '700',
								}}
								variant='body1'
							>
								Autor: {user.username}
							</Typography>
							<Typography>{post.created_at}</Typography>
						</Fragment>
					}
				/>
				<CardMedia
					sx={{
						height: 194,
						width: 880,
						minWidth: '100%',
					}}
					image={post.image.url_image}
					title={post.image.name_image}
				/>
			</CardActionArea>
			<CardContent>
				<Typography component='div' variant='body2' color='text.secondary'>
					<div dangerouslySetInnerHTML={{ __html: post.content }} />
				</Typography>
			</CardContent>
			<CardActions
				disableSpacing
				sx={{
					display: 'flex',
					bgcolor: alpha('#f1f1f1', 0.9),
					position: 'absolute',
					bottom: 0,
					width: '100%',
				}}
			>
				<Button variant='outlined' onClick={props.onClick}>
					Ver la publicación
				</Button>
				<div style={{ flexGrow: 1 }} />
				<IconButton aria-label='favorito'>
					<FavoriteIcon />
				</IconButton>
				<IconButton aria-label='compartir'>
					<ShareIcon />
				</IconButton>
			</CardActions>
		</Card>
	);
}

export default Post;
