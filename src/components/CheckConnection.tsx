import { Fragment, ReactNode, useEffect, useState } from 'react';
import { Backdrop, Box, Typography } from '@material-ui/core';
import SignalWifiOffIcon from '@material-ui/icons/SignalWifiOff';

function CheckConnection(props: { children: ReactNode }) {
	const [checkConnect, setCheckConnect] = useState(true);

	useEffect(() => {
		const onlineEvent = () =>
			window.addEventListener('online', () => {
				setCheckConnect(true);
			});
		const offlineEvent = () =>
			window.addEventListener('offline', () => {
				setCheckConnect(false);
			});

		onlineEvent();
		offlineEvent();

		return () => {
			window.removeEventListener('online', onlineEvent);
			window.removeEventListener('offline', offlineEvent);
		};
	}, []);
	return (
		<Fragment>
			{checkConnect ? (
				props.children
			) : (
				<Fragment>
					<Backdrop
						sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
						open={!checkConnect}
					>
						<Box
							display='flex'
							flexDirection='column'
							justifyContent='space-between'
							alignItems='center'
						>
							<Typography variant='h2' component='h2' align='center' color='orange'>
								Revise su conexión de internet por favor
							</Typography>
							<Typography variant='body1'>(No recargue la aplicación dentro de 5min)</Typography>
							<div>
								<SignalWifiOffIcon fontSize='large' />
							</div>
						</Box>
					</Backdrop>
					{props.children}
				</Fragment>
			)}
		</Fragment>
	);
}

export default CheckConnection;
