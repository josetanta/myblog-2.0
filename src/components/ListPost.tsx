// Hooks
import { usePostList } from 'hooks/usePostQuery';
import { useHistory } from 'react-router-dom';
import * as paths from 'router/paths-routes';
import { Box, Grid } from '@material-ui/core';
import { AnimateSharedLayout, motion } from 'framer-motion';
import Post from './Post';
import withError from 'wrappers/withError';

function ListPost() {
	const postList = usePostList();
	const history = useHistory();
	const handleLinkClick = (postId: string) => {
		history.push(`${paths.POST_PRINCIPAL}/detail/${postId}`);
	};
	return (
		<Box sx={{ flexGrow: 1 }}>
			{postList.isLoading && <div>Cargando</div>}
			{postList.isError && <div>Error</div>}
			<Grid container justifyContent='center' xl={12}>
				<AnimateSharedLayout>
					{postList.isSuccess &&
						postList.data?.posts.map((post) => (
							<motion.div
								initial={{ opacity: 0 }}
								animate={{ opacity: 1 }}
								transition={{
									delay: 1,
									x: {
										type: 'spring',
										stiffness: 100,
									},
									default: { duration: 0.3 },
								}}
								exit={{ opacity: 0 }}
								key={`publicacion-${post.post_uuid}`}
							>
								<Grid item>
									<Post data={post} onClick={() => handleLinkClick(post.id)} />
								</Grid>
							</motion.div>
						))}
				</AnimateSharedLayout>
			</Grid>
		</Box>
	);
}

export default withError(ListPost);
