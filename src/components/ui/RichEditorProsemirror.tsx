import { ChangeEvent, SetStateAction, useState } from 'react';
import { Editor, HtmlEditor, Toolbar } from '@aeaton/react-prosemirror';
import { plugins, schema, toolbar } from '@aeaton/react-prosemirror-config-default';
import useStylesApp from 'utils/styles';
import { Box } from '@material-ui/core';

type RichEditorProps = {
	value: string;
	onChange(text: SetStateAction<string> | ChangeEvent<string>): void;
};

function RichEditorProsemirror(props: RichEditorProps) {
	const classes = useStylesApp({});
	const [value, setValue] = useState(props.value);

	const handleValueChange = (e: string) => {
		setValue(e);
	};

	console.log(value);

	return (
		<Box
			sx={{
				boxShadow: 2,
				py: 1,
				px: 2,
				mt: 2,
			}}
		>
			<HtmlEditor
				schema={schema}
				plugins={plugins}
				value={props.value}
				handleChange={handleValueChange}
				debounce={250}
			>
				<Toolbar className={classes.editorRich} toolbar={toolbar} />
				<Editor autoFocus />
			</HtmlEditor>
		</Box>
	);
}

export default RichEditorProsemirror;
