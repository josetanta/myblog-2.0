import { ChangeEvent, DragEvent, Fragment, memo, useCallback, useState } from 'react';
import { Box, IconButton, Typography } from '@material-ui/core';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import * as colors from '@material-ui/core/colors';
import useStylesApp from 'utils/styles';

type ImageDragDropProps = {
	setFile(file?: File): void;
	file: File | undefined;
	text?: string;
	heigth?: number | string;
	width?: number | string | 'fullWidth';
	imgHeight?: string;
	imgWidth?: string;
};

function ImageDragDrop(props: ImageDragDropProps) {
	const [highlight, setHighlight] = useState(false);
	const classes = useStylesApp({ imgWidth: props.imgWidth, imgHeight: props.imgHeight });
	const handleFileDrop = useCallback(
		(e: DragEvent<HTMLDivElement>) => {
			e.preventDefault();
			e.stopPropagation();
			setHighlight(false);
			let file = e.dataTransfer.files[0];
			props.setFile(file);
		},
		[props]
	);

	const handleFileSelectorClick = (e: ChangeEvent<HTMLInputElement>) => {
		e.preventDefault();
		e.target.files && props.setFile(e.target.files[0]);
	};
	return (
		<Box
			display='flex'
			flexDirection='column'
			component='div'
			position='relative'
			sx={{
				width: props.width,
				height: props?.heigth || 300,
				border: `2px ${highlight ? colors.green['400'] : '#000'} dashed`,
				backgroundColor: highlight ? colors.green['100'] : 'inherit',
				p: 2,
				transition: '0.1s all ease-in',
				':hover': {
					border: `2px ${colors.green['400']} dashed`,
					backgroundColor: colors.green['100'],
				},
			}}
			onDragLeave={() => {
				setHighlight(false);
			}}
			onDragEnter={() => {
				setHighlight(true);
			}}
			onDragOver={(e: DragEvent<HTMLDivElement>) => {
				e.preventDefault();
			}}
			onDrop={handleFileDrop}
		>
			{(props.file && (
				<Fragment>
					<img
						className={classes.imgDrag}
						src={URL.createObjectURL(props.file)}
						alt={props.text || props?.file.name}
					/>
				</Fragment>
			)) || (
				<Typography variant='h6' component='h6' align='center' sx={{ cursor: 'pointer' }}>
					Arrastre su imagen o Presione para seleccionar una imagen
					<input onChange={handleFileSelectorClick} type='file' />
				</Typography>
			)}
			{props.file && (
				<div className={classes.iconCloseFile}>
					<IconButton
						title='Quitar la imagen'
						color='error'
						onClick={() => {
							props.setFile(undefined);
						}}
					>
						<HighlightOffIcon />
					</IconButton>
				</div>
			)}
		</Box>
	);
}

export default memo(ImageDragDrop);
