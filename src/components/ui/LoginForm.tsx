import { Form, Formik } from 'formik';
import { Box, Button, CircularProgress, Paper } from '@material-ui/core';
import { useAuth } from 'hooks/useStores';
import AlertMessage from './AlertMessage';
import FormField from 'components/FormField';
import * as Yup from 'yup';

import LoginIcon from '@material-ui/icons/Login';

type FormValues = {
	email: string;
	password: string;
};

const initialValues: FormValues = {
	email: '',
	password: '',
};

function LoginForm() {
	const [auth, getState] = useAuth();

	return (
		<Paper
			sx={{
				p: 2,
				mt: 2,
			}}
			elevation={2}
		>
			<Formik<FormValues>
				initialValues={initialValues}
				validationSchema={Yup.object({
					email: Yup.string()
						.email('Por favor el correo debe ser correcto.')
						.required('El correo es requerido.'),
					password: Yup.string().required('La contraseña es requerida'),
				})}
				onSubmit={async (values) => await auth.setLoginAuth(values)}
			>
				{() => (
					<Form aria-label='login-form'>
						<Box display='flex' flexDirection='column'>
							<AlertMessage
								show={getState()?.isError}
								message={getState()?.message}
								onClose={auth.setReset}
								variant='outlined'
								severity='error'
							/>

							<Box component='div' sx={{ m: 2 }}>
								<FormField autoFocus required type='email' label='Correo' name='email' />
							</Box>

							<Box component='div' sx={{ m: 2 }}>
								<FormField required type='password' name='password' label='Contraseña' />
							</Box>
							<Box component='div' sx={{ m: 2 }}>
								<Button
									startIcon={<LoginIcon />}
									fullWidth
									type='submit'
									variant='contained'
									disabled={getState().isLoading}
								>
									{(getState().isLoading && <CircularProgress title='Ingresar' size={30} />) ||
										'Ingresar'}
								</Button>
							</Box>
						</Box>
					</Form>
				)}
			</Formik>
		</Paper>
	);
}

export default LoginForm;
