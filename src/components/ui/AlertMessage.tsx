import {
	Alert,
	AlertProps,
	Box,
	Collapse,
	IconButton,
	ListItem,
	ListItemText,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import withError from 'wrappers/withError';

type AlertMessageProps = AlertProps & {
	onClose(): void;
	message?: string | string[] | object[] | object;
	show?: boolean | false;
};

/**
 * Component Alert for message any errors
 * from store
 */
function AlertMessage({ onClose, message, show, ...rest }: AlertMessageProps) {
	return (
		<Collapse in={show}>
			<Box component='div' sx={{ m: 2 }}>
				<Alert
					{...rest}
					action={
						<IconButton aria-label='close' color='inherit' size='small' onClick={onClose}>
							<CloseIcon fontSize='inherit' />
						</IconButton>
					}
				>
					{(message instanceof Array &&
						message.map((obj) => {
							if (obj instanceof Object) {
								let renderObj: Record<string, string> = { ...obj };
								return Object.keys(renderObj).map((key, index) => (
									<ListItem key={`error-${index}`}>
										{renderObj[key].length > 1 && <ListItemText primary={renderObj[key]} />}
									</ListItem>
								));
							} else {
								return (message as Array<string>).map((text, index) => (
									<ListItem key={`error-${index}`}>
										<ListItemText primary={text} />
									</ListItem>
								));
							}
						})) ||
						message}
				</Alert>
			</Box>
		</Collapse>
	);
}

export default withError(AlertMessage);
