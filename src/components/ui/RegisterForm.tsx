import { RouteComponentProps, withRouter } from 'react-router';
import { Form, Formik } from 'formik';
import { Box, Button, Paper } from '@material-ui/core';
import * as Yup from 'yup';
import FormField from 'components/FormField';
import AlertMessage from './AlertMessage';
import { useAuth } from 'hooks/useStores';

type ValueForm = {
	username: string;
	email: string;
	password: string;
	repeatPassword: string;
};

const initValues: ValueForm = {
	username: '',
	email: '',
	password: '',
	repeatPassword: '',
};

function RegisterForm(props: RouteComponentProps) {
	const [auth, getState] = useAuth();
	const history = props.history;
	return (
		<Paper
			sx={{
				p: 2,
				mt: 2,
			}}
			elevation={2}
		>
			<Formik<ValueForm>
				initialValues={initValues}
				onSubmit={async (values, actions) => {
					actions.setSubmitting(true);
					await auth?.setRegisterUser(values);

					if (!auth?.isError) {
						history.push('/sign/in');
					}

					actions.setSubmitting(false);
				}}
				validationSchema={Yup.object({
					email: Yup.string()
						.email('Por favor escribe un correo válido')
						.required('Se require este campo de correo'),
					username: Yup.string()
						.min(5, 'Como mínimo se requiere 5 caracteres')
						.required('Se require este campo de nombre de usuario'),
					password: Yup.string()
						.min(8, 'La nueva contraseña como mínimo debe ser de 8 caracteres.')
						.required('Se require la contraseña.'),
					repeatPassword: Yup.string()
						.equals([Yup.ref('password')], 'Las contraseñas deben de ser iguales.')
						.required('Es requerido repetir la contraseña'),
				})}
			>
				{({ isSubmitting }) => (
					<Form>
						<Box display='flex' flexDirection='column'>
							{auth?.isError && (
								<Box component='div' sx={{ m: 2 }}>
									<AlertMessage
										show={getState()?.isError}
										message={getState()?.message}
										onClose={auth.setReset}
										variant='outlined'
									/>
								</Box>
							)}
							<Box component='div' sx={{ m: 2 }}>
								<FormField required name='email' label='Correo' type='email' />
							</Box>
							<Box component='div' sx={{ m: 2 }}>
								<FormField required name='username' label='Nombre de usuario' type='text' />
							</Box>
							<Box component='div' sx={{ m: 2 }}>
								<FormField required name='password' label='Contraseña' type='password' />
							</Box>
							<Box component='div' sx={{ m: 2 }}>
								<FormField
									required
									name='repeatPassword'
									label='Repite la contraseña'
									type='password'
								/>
							</Box>

							<Box component='div' sx={{ m: 2 }}>
								<Button type='submit' disabled={isSubmitting} fullWidth variant='contained'>
									Registrarse con esta cuenta
								</Button>
							</Box>
						</Box>
					</Form>
				)}
			</Formik>
		</Paper>
	);
}

export default withRouter(RegisterForm);
