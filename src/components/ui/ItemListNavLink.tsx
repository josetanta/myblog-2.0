import { ReactNode } from 'react';
import { NavLink } from 'react-router-dom';
import { ListItem, ListItemIcon, ListItemText, useTheme } from '@material-ui/core';

type ItemListNLProps = { nameItem: string; href: string; icon?: ReactNode };

function ItemListNavLink(props: ItemListNLProps) {
	const theme = useTheme();
	return (
		<ListItem
			button
			to={props.href}
			activeStyle={{
				fontWeight: 'bolder',
				color: theme.palette.getContrastText(theme.palette.primary.main),
				backgroundColor: theme.palette.primary.main,
			}}
			component={NavLink}
		>
			<ListItemIcon>{props.icon}</ListItemIcon>
			<ListItemText primary={props.nameItem} />
		</ListItem>
	);
}

export default ItemListNavLink;
