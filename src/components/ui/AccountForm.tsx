import { Box, Button, Typography } from '@material-ui/core';
import { useUserSettings } from 'context/SettingsContext';
import FormField from 'components/FormField';
import { Form, Formik } from 'formik';

import * as Yup from 'yup';
import ImageDragDrop from './ImageDragDrop';
import { valuesObject } from 'utils/formData';
import { useAuth } from 'hooks/useStores';

type ValueForm = {
	username: string;
	email?: string;
	phone?: string;
	address?: string;
	address_2?: string;
};

function AccountForm() {
	const userSettings = useUserSettings();
	const [auth, getState] = useAuth();

	const initValues: ValueForm = {
		address: getState().user?.address || '',
		address_2: getState().user?.address_2 || '',
		username: getState().user?.username || '',
		email: getState().user?.email || '',
		phone: getState().user?.phone || '',
	};

	return (
		<Formik<ValueForm>
			initialValues={initValues}
			validationSchema={Yup.object({
				username: Yup.string()
					.min(5, 'Debe ser como mínimo de 5 caracteres')
					.max(20, 'Debe ser como máximo de 15 caracteres'),
				email: Yup.string().email('Por favor el email debe ser correcto.'),
				phone: Yup.number().min(9, 'El numero de telefono debe ser de 9 numeros'),
			})}
			onSubmit={async (values, actions) => {
				actions.setSubmitting(true);
				const formData = valuesObject(values);
				if (userSettings.state.file?.name) formData.append('image', userSettings.state.file);

				await auth.setUpdateAccount(formData);

				actions.setSubmitting(false);
			}}
		>
			{({ values, isSubmitting }) => (
				<Form>
					<Box display='flex' flexDirection='column'>
						<Box
							display='flex'
							flexDirection='column'
							alignItems='center'
							justifyContent='space-content'
							component='div'
							sx={{ m: 2, mb: 2 }}
						>
							<ImageDragDrop
								imgHeight='50%'
								imgWidth='50%'
								heigth='calc(100% - 5px)'
								width='calc(100% - 1rem)'
								file={userSettings.state.file}
								setFile={(e) => userSettings.dispatch({ type: 'SUBMIT_FILE', payload: e })}
								text={values.username}
							/>
						</Box>
						{(getState().authName?.length && (
							<Box sx={{ m: 2, borderColor: 'primary.main' }}>
								<strong>Email</strong>:
								<Typography variant='h6'>{getState().user?.email}</Typography>
							</Box>
						)) || (
							<Box component='div' sx={{ m: 2 }}>
								<FormField required name='email' label='Correo' type='email' />
							</Box>
						)}

						<Box component='div' sx={{ m: 2 }}>
							<FormField required name='username' label='Nombre de usuario' type='text' />
						</Box>
						<Box component='div' sx={{ m: 2 }}>
							<FormField name='address' label='Dirección' type='text' />
						</Box>
						<Box component='div' sx={{ m: 2 }}>
							<FormField name='address_2' label='Dirección Alterna' type='text' />
						</Box>
						<Box component='div' sx={{ m: 2 }}>
							<FormField name='phone' label='Teléfono' type='text' />
						</Box>
						<Box component='div' sx={{ m: 2 }}>
							<Button type='submit' fullWidth disabled={isSubmitting} variant='contained'>
								Actualizar mi cuenta
							</Button>
						</Box>
					</Box>
				</Form>
			)}
		</Formik>
	);
}

export default AccountForm;
