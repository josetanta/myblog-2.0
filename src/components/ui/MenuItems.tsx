import { KeyboardEvent, ReactNode, SyntheticEvent, useEffect, useRef, useState } from 'react';
import {
	Button,
	ClickAwayListener,
	Grow,
	ListItemIcon,
	MenuItem,
	MenuList,
	Paper,
	Popper,
} from '@material-ui/core';

// Hooks
import { useAuth } from 'hooks/useStores';

import { RouteComponentProps, withRouter } from 'react-router-dom';
import * as paths from 'router/paths-routes';
import * as colors from '@material-ui/core/colors';

// Icons
import LogoutIcon from '@material-ui/icons/Logout';
import SettingsIcon from '@material-ui/icons/Settings';
import PersonIcon from '@material-ui/icons/Person';
import LoginIcon from '@material-ui/icons/Login';

type MenuItemsProps = {
	id: string;
	titleMenu?: string;
	icon?: ReactNode;
};

function MenuItems(props: MenuItemsProps & RouteComponentProps) {
	const history = props.history;
	const [auth, getState] = useAuth();
	const [openMenu, setOpenMenu] = useState(false);
	const anchorRef = useRef<HTMLButtonElement>(null);

	const handleToggleClick = () => {
		setOpenMenu((prevOpen) => !prevOpen);
	};

	const handleCloseClick = (e: Event | SyntheticEvent) => {
		if (anchorRef.current && anchorRef.current.contains(e.target as HTMLElement)) {
			return;
		}
		setOpenMenu(false);
	};

	const handleLogoutUserClick = async () => {
		await auth.setLogoutAuth();
	};

	const handleListItemsToggleKeyDown = (e: KeyboardEvent) => {
		if (e.key === 'Tab') {
			e.preventDefault();
			setOpenMenu(false);
		} else if (e.key === 'Escape') {
			setOpenMenu(false);
		}
	};

	const handleReplaceRouterClick = () => {
		history.replace('/sign/in');
	};

	const prevOpenRef = useRef(openMenu);

	useEffect(() => {
		if (prevOpenRef.current && !openMenu) {
			anchorRef.current!.focus();
		}
		prevOpenRef.current = openMenu;
	}, [openMenu]);

	const renderImage = () => {
		if (getState().isAuthenticated) {
			return (
				<img
					src={getState().user?.thumbnail.url_image}
					alt={getState().user?.thumbnail.name_image}
					width={30}
					height={30}
				/>
			);
		} else if (!getState().user?.thumbnail.url_image) {
			return props.icon;
		}

		return props.icon;
	};

	return (
		<div>
			<Button
				color='inherit'
				ref={anchorRef}
				aria-controls={openMenu ? props.id : undefined}
				aria-expanded={openMenu ? 'true' : undefined}
				aria-haspopup='true'
				onClick={handleToggleClick}
				startIcon={renderImage()}
			>
				{getState().isAuthenticated && getState().user?.username}
				{!getState().isAuthenticated && props.titleMenu?.length && props.titleMenu}
			</Button>

			<Popper
				open={openMenu}
				anchorEl={anchorRef.current}
				role={undefined}
				placement='bottom-start'
				transition
				disablePortal
			>
				{({ TransitionProps, placement }) => (
					<Grow
						{...TransitionProps}
						style={{
							transformOrigin: placement === 'bottom-start' ? 'left top' : 'left bottom',
						}}
					>
						<Paper>
							<ClickAwayListener onClickAway={handleCloseClick}>
								<MenuList
									autoFocusItem={openMenu}
									id={props.id}
									aria-labelledby={props.id}
									onKeyDown={handleListItemsToggleKeyDown}
								>
									{(getState().isAuthenticated && (
										<div>
											<MenuItem
												onClick={(e) => {
													handleCloseClick(e);
													history.push(paths.USER_PRINCIPAL);
												}}
											>
												<ListItemIcon>
													<PersonIcon />
												</ListItemIcon>
												Mi Perfil
											</MenuItem>
											<MenuItem onClick={handleCloseClick}>
												<ListItemIcon>
													<SettingsIcon />
												</ListItemIcon>
												Configuración
											</MenuItem>
											<MenuItem
												sx={{
													backgroundColor: colors.red['200'],
												}}
												onClick={(e) => {
													handleCloseClick(e);
													(async () => {
														await handleLogoutUserClick();
													})();
												}}
											>
												<ListItemIcon>
													<LogoutIcon />
												</ListItemIcon>
												Cerrar Sesión
											</MenuItem>
										</div>
									)) || (
										<MenuItem
											onClick={(e) => {
												handleCloseClick(e);
												handleReplaceRouterClick();
											}}
										>
											<ListItemIcon>
												<LoginIcon />
											</ListItemIcon>
											Iniciar sesión
										</MenuItem>
									)}
								</MenuList>
							</ClickAwayListener>
						</Paper>
					</Grow>
				)}
			</Popper>
		</div>
	);
}

export default withRouter(MenuItems);
