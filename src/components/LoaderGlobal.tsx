import { useIsFetching } from 'react-query';
import { CircularProgress } from '@material-ui/core';

function LoaderGlobal() {
	const isFetching = useIsFetching();
	return (
		<CircularProgress
			sx={{
				position: 'absolute',
				top: '5rem',
				right: '2rem',
				animationDuration: '0.5s',
				visibility: isFetching ? 'visible' : 'hidden',
			}}
			size={20}
			color='primary'
		/>
	);
}

export default LoaderGlobal;
