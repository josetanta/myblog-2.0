import { FC } from 'react';
import { TextField } from '@material-ui/core';
import { ErrorMessage, FieldAttributes, useField } from 'formik';

type FormFieldProps = {
	label: string;
	type: 'text' | 'email' | 'password';
	required?: boolean | true;
} & FieldAttributes<{}>;

const FormField: FC<FormFieldProps> = ({ label, type, required, ...props }) => {
	const [field, meta] = useField<{}>(props.name);
	const isError = Boolean(meta.error?.length && meta.touched);

	return (
		<TextField
			{...field}
			label={label}
			type={type}
			required={required}
			fullWidth
			error={isError}
			helperText={<ErrorMessage name={field.name} component='span' />}
		/>
	);
};
export default FormField;
