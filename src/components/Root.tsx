import { Fragment, ReactNode } from 'react';
import { useAuth } from 'hooks/useStores';
import { Typography } from '@material-ui/core';
import useMounted from 'hooks/useMounted';
import useDocumentTitle from 'hooks/useDocumentTitle';

function Root(props: { children: ReactNode }) {
	const [auth] = useAuth();

	useDocumentTitle('Cargando...')
	useMounted(async (isMounted) => {
		if (isMounted) {
			await auth.getUserAccount();
			await auth.setIsReady(true);
		}
	});
	if (!auth.isReady) {
		return (
			<Typography variant='h1' align='center' marginY='auto'>
				Cargando...
			</Typography>
		);
	}

	return <Fragment>{props.children}</Fragment>;
}

export default Root;
