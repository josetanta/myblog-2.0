import { Redirect, Route, RouteComponentProps, Switch } from 'react-router-dom';
import { AnimatePresence } from 'framer-motion';

import AccountPage from 'pages/users/AccountPage';
import * as paths from './paths-routes';
import MyPostsPage from 'pages/users/MyPostsPage';
import NotFoundPage from 'pages/errors/NotFoundPage';
import AccountConfigPage from 'pages/users/AccountConfigPage';
import ChangePassPage from 'pages/users/ChangePassPage';

function UserRouter(props: RouteComponentProps) {
	const location = props.location;
	const match = props.match;
	return (
		<AnimatePresence exitBeforeEnter key={match.url}>
			<Switch location={location} key={location.pathname}>
				<Route path={`${match.url}${paths.USER_ACCOUNT}`} component={AccountPage} />
				<Route path={`${match.url}${paths.USER_SETTINGS}`} component={AccountConfigPage} />
				<Route path={`${match.url}${paths.USER_MY_POSTS}`} component={MyPostsPage} />
				<Route path={`${match.url}${paths.USER_CHANGE_PASS}`} component={ChangePassPage} />

				<Redirect
					exact
					from={paths.USER_PRINCIPAL}
					to={`${paths.USER_PRINCIPAL}${paths.USER_ACCOUNT}`}
				/>
				<Route exact path={`${paths.USER_PRINCIPAL}${paths.NOT_FOUND}`} component={NotFoundPage} />
			</Switch>
		</AnimatePresence>
	);
}

export default UserRouter;
