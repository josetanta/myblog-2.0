import { Route, Switch, useLocation } from 'react-router-dom';
import { AnimatePresence } from 'framer-motion';
import * as paths from './paths-routes';

// Routers
import PrivateRouter from './PrivateRouter';
import UserRouter from './UserRouter';
import PostRouter from './PostRouter';

// Pages
import SignInAndUpPage from 'pages/SignInAndUpPage';
import NotFoundPage from 'pages/errors/NotFoundPage';
import ForbiddenPage from 'pages/errors/ForbiddenPage';
import HomePage from 'pages/HomePage';
import AboutPage from 'pages/AboutPage';

function AppRouter() {
	const location = useLocation();
	return (
		<AnimatePresence exitBeforeEnter>
			<Switch location={location} key={`principal-${location.pathname}`}>
				<Route path={paths.ABOUT} component={AboutPage} />
				<Route path={paths.SIGN_IN} component={SignInAndUpPage} />
				<Route path={paths.REGISTER} component={SignInAndUpPage} />
				<Route path={paths.POST_PRINCIPAL} component={PostRouter} />

				<PrivateRouter path={paths.USER_PRINCIPAL} component={UserRouter} />

				<PrivateRouter path={paths.FORBIDDEN} component={ForbiddenPage} />

				<Route exact strict path={paths.HOME} component={HomePage} />

				<Route exact path={paths.NOT_FOUND} component={NotFoundPage} />
			</Switch>
		</AnimatePresence>
	);
}

export default AppRouter;
