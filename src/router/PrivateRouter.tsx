import { useAuth } from 'hooks/useStores';
import { Redirect, Route, RouteProps } from 'react-router-dom';

import * as paths from './paths-routes';

function PrivateRouter(props: RouteProps) {
	const [auth] = useAuth();

	if (!auth.isAuthenticated) {
		return (
			<Redirect
				exact
				to={{
					pathname: paths.SIGN_IN,
					state: { from: props.location },
				}}
			/>
		);
	}
	return <Route {...props} />;
}

export default PrivateRouter;
