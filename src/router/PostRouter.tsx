import { Redirect, Route, RouteComponentProps, Switch } from 'react-router-dom';
import { AnimatePresence } from 'framer-motion';

import * as paths from 'router/paths-routes';
import PostDetailPage from 'pages/posts/PostDetailPage';
import PostPrincipal from 'pages/posts/PostPrincipal';
import CreatePostPage from 'pages/posts/CreatePostPage';
import PrivateRouter from './PrivateRouter';

function PostRouter(props: RouteComponentProps) {
	const location = props.location;
	const routeMatch = props.match;
	return (
		<AnimatePresence exitBeforeEnter>
			<Switch location={location} key={`posts-${location.pathname}`}>
				<Route path={`${routeMatch.url}${paths.POST_DETAIL}`} component={PostDetailPage} />
				<PrivateRouter path={`${routeMatch.url}${paths.POST_CREATE}`} component={CreatePostPage} />

				<Route path={paths.POST_PRINCIPAL} component={PostPrincipal} />
				<Redirect from={paths.POST_PRINCIPAL} to={routeMatch.url} exact />
			</Switch>
		</AnimatePresence>
	);
}

export default PostRouter;
